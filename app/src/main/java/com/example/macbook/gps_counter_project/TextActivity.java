package com.example.macbook.gps_counter_project;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class TextActivity extends AppCompatActivity {

    private TextView textPreview;
    private String textFromFile = " ";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_text);

        textPreview = findViewById(R.id.text_preview);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            textFromFile = extras.getString("textFile");
            textPreview.setText(textFromFile);
        }



    }
}
