package com.example.macbook.gps_counter_project;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.session.MediaSession;
import android.media.session.PlaybackState;
import android.os.Bundle;
import android.os.SystemClock;
import android.os.Vibrator;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Chronometer;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;
import java.util.TimeZone;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageButton pauseBtn, stopBtn, leftBtn, rightBtn, playBtnAfterPause, openInfo;
    private LinearLayout bottomLayout;
    private TextView txtLeftCount, txtRightCount;

    private long pauseOffset = 0;
    private boolean runningTime;

    private Chronometer chronometer;

    private int leftClickedNumber = 0;
    private int rightClickedNumber = 0;

    private LocationManager mLocManager;
    private LocationListener locationListener;
    private LogWriter logWriter;
    private MediaSession mediaSession;
    private boolean isPlaying = false;

    private Vibrator vibe;
    private MediaPlayer mPlayer;

    private boolean isPaused = false;


    private final String[] PERMISSIONS_LIST = {
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        logWriter = new LogWriter();
        mPlayer = MediaPlayer.create(MainActivity.this, R.raw.light);

        initViews();

        vibe = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

        ActivityCompat.requestPermissions(MainActivity.this,
                PERMISSIONS_LIST,
                1);

        setMediaButtons();


    }

    private void setListeners() {
        pauseBtn.setOnClickListener(this);
        playBtnAfterPause.setOnClickListener(this);
        stopBtn.setOnClickListener(this);
        leftBtn.setOnClickListener(this);
        rightBtn.setOnClickListener(this);
        openInfo.setOnClickListener(this);
    }


    @SuppressLint("SetTextI18n")
    private void setChronometer() {

        chronometer.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener() {
            @Override
            public void onChronometerTick(Chronometer chronometer) {
                mPlayer.start();
                long time = SystemClock.elapsedRealtime() - chronometer.getBase();
                int h = (int) (time / 3600000);
                int m = (int) (time - h * 3600000) / 60000;
                int s = (int) (time - h * 3600000 - m * 60000) / 1000;
                String t = (h < 10 ? "0" + h : h) + ":" + (m < 10 ? "0" + m : m) + ":" + (s < 10 ? "0" + s : s);
                chronometer.setText(t);
            }
        });

        chronometer.setBase(SystemClock.elapsedRealtime());
        chronometer.setText("00:00:00");
    }

    private void startTakingGPSPoints() {
        mLocManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        locationListener = new LocationListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onLocationChanged(Location location) {

                String timestamp = "";
                try {

                    timestamp = milisecondsIntoDate().toString();
                    timestamp = timestamp.replace("GMT+04:00", "");
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                if(!isPaused) {
                    logWriter.writeToFile(timestamp + "\t" + location.getLatitude() + "\t" + location.getLongitude() + "\t" + location.getBearing() + "\t" + "B" + "\r\n");
                }
            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {

            }

            @Override
            public void onProviderEnabled(String s) {

            }

            @Override
            public void onProviderDisabled(String s) {

            }
        };

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            onDestroy();
            return;
        }

        mLocManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 0, locationListener);

    }

    private Date milisecondsIntoDate() throws ParseException {

        @SuppressLint("SimpleDateFormat") SimpleDateFormat dateFormatGmt = new SimpleDateFormat("yyyy-MMM-dd HH:mm:ss");
        dateFormatGmt.setTimeZone(TimeZone.getTimeZone("GMT-04:00"));

//Local time zone
        @SuppressLint("SimpleDateFormat") SimpleDateFormat dateFormatLocal = new SimpleDateFormat("yyyy-MMM-dd HH:mm:ss");

//Time in GMT
        return dateFormatLocal.parse(dateFormatGmt.format(new Date()));
    }

    private void pauseCounting() {
        if (runningTime) {
            chronometer.stop();
            pauseOffset = SystemClock.elapsedRealtime() - chronometer.getBase();
            runningTime = false;

        }
    }

    private void stopCounting() {
        mLocManager.removeUpdates(locationListener);
        chronometer.setText("00:00:00");
        chronometer.stop();
        pauseOffset = 0;
        leftClickedNumber = 0;
        txtLeftCount.setText(" 0 ");
        rightClickedNumber = 0;
        txtRightCount.setText(" 0 ");
    }

    private void startCountingTime() {
        if (!runningTime) {
            chronometer.setBase(SystemClock.elapsedRealtime() - pauseOffset);
            chronometer.start();
            runningTime = true;
        }
    }

    private void initViews() {
        pauseBtn = findViewById(R.id.pauseBtn);
        stopBtn = findViewById(R.id.stopBtn);
        leftBtn = findViewById(R.id.leftBtn);
        rightBtn = findViewById(R.id.rightBtn);
        playBtnAfterPause = findViewById(R.id.play_after_pause_btn);
        openInfo = findViewById(R.id.open_file);

        bottomLayout = findViewById(R.id.bottom_layout);
        txtLeftCount = findViewById(R.id.left_count);
        txtRightCount = findViewById(R.id.right_count);

        chronometer = findViewById(R.id.rec_time);
    }

    @SuppressLint({"MissingPermission", "SetTextI18n"})
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.pauseBtn:
                pauseBtnFunc();
                break;
            case R.id.play_after_pause_btn:
                playBtnFunc();
                break;
            case R.id.stopBtn:
                stopBtnFunc();
                break;
            case R.id.leftBtn:
                leftBtnFunc();
                break;
            case R.id.rightBtn:
                rightBtnFunc();
                break;
            case R.id.open_file:
                Intent intent = new Intent(this, TextActivity.class);

                String text = getTextFromFile(logWriter.getFile());
                intent.putExtra("textFile", text);

                startActivity(intent);
                break;

        }
    }

    @SuppressLint("SetTextI18n")
    private void rightBtnFunc() {
        if (!isPlaying) {
            mLocManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            LocationListener rightListener = new LocationListener() {
                @Override
                public void onLocationChanged(Location location) {

                }

                @Override
                public void onStatusChanged(String provider, int status, Bundle extras) {

                }

                @Override
                public void onProviderEnabled(String provider) {

                }

                @Override
                public void onProviderDisabled(String provider) {

                }
            };

            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            mLocManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 0, rightListener);

        }
        vibe.vibrate(100);
        playButtonSound();

            @SuppressLint("MissingPermission") Location locationRight = mLocManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

            String timestampRight = "";
            try {
                timestampRight = milisecondsIntoDate().toString();
                timestampRight = timestampRight.replace("GMT+04:00", "");

            } catch (ParseException e) {
                e.printStackTrace();
            }
            if (locationRight != null) {
                rightClickedNumber++;
                txtRightCount.setText(" " + rightClickedNumber + " ");
                logWriter.writeToFile(timestampRight + "\t" + locationRight.getLatitude() + "\t" + locationRight.getLongitude() + "\t" + locationRight.getBearing() + "\t" + "R" + "\r\n");
            } else {
                Toast.makeText(this, "Sorry, you didn't get any location yet", Toast.LENGTH_SHORT).show();
            }


    }

    @SuppressLint("SetTextI18n")
    private void leftBtnFunc() {
        if(!isPlaying) {
            mLocManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        }
        vibe.vibrate(100);
        playButtonSound();



            @SuppressLint("MissingPermission") Location locationLeft = mLocManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

            String timestampLeft = "";
            try {
                timestampLeft = milisecondsIntoDate().toString();
                timestampLeft = timestampLeft.replace("GMT+04:00", "");

            } catch (ParseException e) {
                e.printStackTrace();
            }
            if (locationLeft != null) {
                leftClickedNumber++;
                txtLeftCount.setText(" " + leftClickedNumber + " ");
                logWriter.writeToFile(timestampLeft + "\t" + locationLeft.getLatitude() + "\t" + locationLeft.getLongitude() + "\t" + locationLeft.getBearing() + "\t" + "L" + "\r\n");
            } else {
                Toast.makeText(this, "Sorry, you didn't get any location yet", Toast.LENGTH_SHORT).show();
            }

    }

    private void stopBtnFunc() {
        isPlaying = false;
        runningTime = false;

        mPlayer.stop();
        mPlayer.reset();
        mPlayer = MediaPlayer.create(MainActivity.this, R.raw.light);


        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        playButtonSound();

        playBtnAfterPause.setVisibility(View.VISIBLE);
        pauseBtn.setVisibility(View.GONE);

        stopCounting();
    }

    private void pauseBtnFunc() {
        isPlaying = false;
        isPaused = true;
        mPlayer.stop();
        mPlayer.reset();
        mPlayer = MediaPlayer.create(MainActivity.this, R.raw.light);

        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        playButtonSound();
        pauseBtn.setVisibility(View.GONE);
        playBtnAfterPause.setVisibility(View.VISIBLE);
        pauseCounting();
    }

    private void playBtnFunc() {
        isPlaying = true;
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        playButtonSound();

        playBtnAfterPause.setVisibility(View.GONE);
        bottomLayout.setVisibility(View.VISIBLE);
        pauseBtn.setVisibility(View.VISIBLE);

        startCountingTime();
        startTakingGPSPoints();
    }

    private void setMediaButtons() {
        mediaSession = new MediaSession(this, "TAGS");
        mediaSession.setActive(true);
        PlaybackState state = new PlaybackState.Builder()
                .setActions(PlaybackState.ACTION_PLAY)
                .setState(PlaybackState.STATE_PLAYING, PlaybackState.PLAYBACK_POSITION_UNKNOWN, SystemClock.elapsedRealtime())
                .build();
        mediaSession.setFlags(MediaSession.FLAG_HANDLES_MEDIA_BUTTONS | MediaSession.FLAG_HANDLES_TRANSPORT_CONTROLS);
        mediaSession.setPlaybackState(state);
        mediaSession.setCallback(new MediaSession.Callback() {

            @Override
            public boolean onMediaButtonEvent(@NonNull Intent mediaButtonIntent) {
                if (Intent.ACTION_MEDIA_BUTTON.equals(mediaButtonIntent.getAction())) {
                    KeyEvent ke = mediaButtonIntent.getParcelableExtra(Intent.EXTRA_KEY_EVENT);
                    if (ke != null && ke.getAction() == KeyEvent.ACTION_DOWN) {
                        switch (ke.getKeyCode()) {

                            case KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE:
                            case KeyEvent.KEYCODE_MEDIA_PAUSE:
                            case KeyEvent.KEYCODE_MEDIA_PLAY:
                            case KeyEvent.KEYCODE_HEADSETHOOK:
                                if (isPlaying) {

                                    pauseBtnFunc();
                                } else {
                                    playBtnFunc();
                                }
                                break;
                            case KeyEvent.KEYCODE_MEDIA_STOP:
                                stopBtnFunc();
                                break;

                            case KeyEvent.KEYCODE_MEDIA_NEXT:
                            case KeyEvent.KEYCODE_MEDIA_SKIP_FORWARD:
                            case KeyEvent.KEYCODE_MEDIA_STEP_FORWARD:
                            case KeyEvent.KEYCODE_MEDIA_FAST_FORWARD:
                                rightBtnFunc();
                                break;

                            case KeyEvent.KEYCODE_MEDIA_PREVIOUS:
                            case KeyEvent.KEYCODE_MEDIA_SKIP_BACKWARD:
                            case KeyEvent.KEYCODE_MEDIA_STEP_BACKWARD:
                            case KeyEvent.KEYCODE_MEDIA_REWIND:
                                leftBtnFunc();
                                break;

                        }
                    }
                    return true;
                }
                return false;
            }

        });
        mediaSession.setActive(true);
        setMediaController(mediaSession.getController());
    }

    private String getTextFromFile(File file) {
        //Read text from file
        StringBuilder text = new StringBuilder();

        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line;

            while ((line = br.readLine()) != null) {
                text.append(line);
                text.append('\n');
            }
            br.close();
        } catch (IOException e) {
            //You'll need to add proper error handling here
        }

        return text.toString();
    }

    private void playButtonSound() {
        AudioManager audioManager =
                (AudioManager) this.getSystemService(Context.AUDIO_SERVICE);

        Objects.requireNonNull(audioManager).playSoundEffect(AudioManager.FLAG_PLAY_SOUND, 0.8f);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case 1: {
                if (grantResults.length > 0) {
                    for (int grantResult : grantResults) {
                        if (grantResult == PackageManager.PERMISSION_GRANTED) {
                            // permission was granted
                            setListeners();
                            setChronometer();
                        } else {
                            finish();
                        }
                    }


                } else {
                    // permission denied , app closing
                    finish();
                }
            }

        }
    }

    @Override
    protected void onDestroy() {
        mediaSession.setActive(false);
        super.onDestroy();
    }
}