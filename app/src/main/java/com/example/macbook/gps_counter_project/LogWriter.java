package com.example.macbook.gps_counter_project;

import android.os.Environment;
import android.util.Log;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class LogWriter {

    private Executor fileWriteExecutor;
    private File locationFile;

    LogWriter() {

        fileWriteExecutor = Executors.newSingleThreadExecutor();

        fileWriteExecutor.execute(new Runnable() {
            @Override
            public void run() {

                SimpleDateFormat simpleDateFormat= new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
                simpleDateFormat.setTimeZone(TimeZone.getTimeZone("EST"));
                String s = simpleDateFormat.format(new Date(System.currentTimeMillis()));


                final File path = new File(Environment.getExternalStorageDirectory() + File.separator + "LocationsFolder");
                locationFile = new File(path, s + "EST" + ".txt");
                try {
                    path.mkdirs();
                    locationFile.createNewFile();
                } catch (IOException e) {
                    Log.e("Exception", "File write failed: " + e.toString());
                }
            }
        });
    }


    public void writeToFile(final String data) {
        fileWriteExecutor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(locationFile, true)));
                    out.println(data);
                    out.flush();
                    out.close();

                } catch (IOException e) {
                    Log.e("Exception", "File write failed: " + e.toString());
                }
            }
        });

    }

    public File getFile() {
        return locationFile;
    }
}
